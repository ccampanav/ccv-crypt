﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Out
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Out))
        Me.LCharOut = New System.Windows.Forms.Label()
        Me.RTBOut = New System.Windows.Forms.RichTextBox()
        Me.LTitle = New System.Windows.Forms.Label()
        Me.BCopiar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LCharOut
        '
        Me.LCharOut.BackColor = System.Drawing.Color.Transparent
        Me.LCharOut.Location = New System.Drawing.Point(329, 381)
        Me.LCharOut.Name = "LCharOut"
        Me.LCharOut.Size = New System.Drawing.Size(140, 16)
        Me.LCharOut.TabIndex = 11
        Me.LCharOut.Text = "Caracteres: 0"
        Me.LCharOut.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'RTBOut
        '
        Me.RTBOut.BackColor = System.Drawing.Color.White
        Me.RTBOut.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBOut.DetectUrls = False
        Me.RTBOut.ForeColor = System.Drawing.Color.DimGray
        Me.RTBOut.Location = New System.Drawing.Point(12, 57)
        Me.RTBOut.MaxLength = 1000000
        Me.RTBOut.Name = "RTBOut"
        Me.RTBOut.ReadOnly = True
        Me.RTBOut.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.RTBOut.Size = New System.Drawing.Size(460, 310)
        Me.RTBOut.TabIndex = 10
        Me.RTBOut.Text = ""
        '
        'LTitle
        '
        Me.LTitle.BackColor = System.Drawing.Color.Transparent
        Me.LTitle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LTitle.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTitle.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LTitle.Location = New System.Drawing.Point(7, 10)
        Me.LTitle.Name = "LTitle"
        Me.LTitle.Size = New System.Drawing.Size(470, 40)
        Me.LTitle.TabIndex = 13
        Me.LTitle.Text = "Texto Generado"
        Me.LTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BCopiar
        '
        Me.BCopiar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BCopiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BCopiar.Location = New System.Drawing.Point(183, 376)
        Me.BCopiar.Name = "BCopiar"
        Me.BCopiar.Size = New System.Drawing.Size(120, 28)
        Me.BCopiar.TabIndex = 15
        Me.BCopiar.Text = "Copiar texto" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.BCopiar.UseVisualStyleBackColor = True
        '
        'Out
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(484, 411)
        Me.Controls.Add(Me.BCopiar)
        Me.Controls.Add(Me.LTitle)
        Me.Controls.Add(Me.LCharOut)
        Me.Controls.Add(Me.RTBOut)
        Me.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Out"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CCV Crypt"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LCharOut As Label
    Friend WithEvents RTBOut As RichTextBox
    Friend WithEvents LTitle As Label
    Friend WithEvents BCopiar As Button
End Class
