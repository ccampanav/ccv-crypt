﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.RTBIn = New System.Windows.Forms.RichTextBox()
        Me.LCharIn = New System.Windows.Forms.Label()
        Me.LKey = New System.Windows.Forms.Label()
        Me.LTextGenerated = New System.Windows.Forms.Label()
        Me.LCifrar = New System.Windows.Forms.Label()
        Me.LDescifrar = New System.Windows.Forms.Label()
        Me.PBIcon = New System.Windows.Forms.PictureBox()
        Me.PBSpinner = New System.Windows.Forms.PictureBox()
        Me.T1 = New System.Windows.Forms.Timer(Me.components)
        Me.ProgressBarT1 = New System.Windows.Forms.ProgressBar()
        Me.RTBOutAux = New System.Windows.Forms.RichTextBox()
        CType(Me.PBIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBSpinner, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RTBIn
        '
        Me.RTBIn.BackColor = System.Drawing.Color.White
        Me.RTBIn.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBIn.DetectUrls = False
        Me.RTBIn.ForeColor = System.Drawing.Color.DimGray
        Me.RTBIn.Location = New System.Drawing.Point(17, 103)
        Me.RTBIn.MaxLength = 80000
        Me.RTBIn.Name = "RTBIn"
        Me.RTBIn.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.RTBIn.Size = New System.Drawing.Size(550, 330)
        Me.RTBIn.TabIndex = 5
        Me.RTBIn.Text = ""
        '
        'LCharIn
        '
        Me.LCharIn.BackColor = System.Drawing.Color.Transparent
        Me.LCharIn.Location = New System.Drawing.Point(428, 446)
        Me.LCharIn.Name = "LCharIn"
        Me.LCharIn.Size = New System.Drawing.Size(140, 16)
        Me.LCharIn.TabIndex = 8
        Me.LCharIn.Text = "Caracteres: 0"
        Me.LCharIn.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LKey
        '
        Me.LKey.BackColor = System.Drawing.Color.White
        Me.LKey.ForeColor = System.Drawing.Color.DimGray
        Me.LKey.Location = New System.Drawing.Point(18, 445)
        Me.LKey.Name = "LKey"
        Me.LKey.Size = New System.Drawing.Size(100, 15)
        Me.LKey.TabIndex = 10
        Me.LKey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LKey.Visible = False
        '
        'LTextGenerated
        '
        Me.LTextGenerated.BackColor = System.Drawing.Color.Transparent
        Me.LTextGenerated.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LTextGenerated.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTextGenerated.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LTextGenerated.Location = New System.Drawing.Point(142, 446)
        Me.LTextGenerated.Name = "LTextGenerated"
        Me.LTextGenerated.Size = New System.Drawing.Size(300, 24)
        Me.LTextGenerated.TabIndex = 14
        Me.LTextGenerated.Text = "Ver texto generado"
        Me.LTextGenerated.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LTextGenerated.Visible = False
        '
        'LCifrar
        '
        Me.LCifrar.AutoSize = True
        Me.LCifrar.BackColor = System.Drawing.Color.Transparent
        Me.LCifrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LCifrar.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCifrar.ForeColor = System.Drawing.Color.DimGray
        Me.LCifrar.Location = New System.Drawing.Point(121, 32)
        Me.LCifrar.Name = "LCifrar"
        Me.LCifrar.Size = New System.Drawing.Size(93, 36)
        Me.LCifrar.TabIndex = 15
        Me.LCifrar.Text = "Cifrar"
        Me.LCifrar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LDescifrar
        '
        Me.LDescifrar.AutoSize = True
        Me.LDescifrar.BackColor = System.Drawing.Color.Transparent
        Me.LDescifrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LDescifrar.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LDescifrar.ForeColor = System.Drawing.Color.DimGray
        Me.LDescifrar.Location = New System.Drawing.Point(382, 32)
        Me.LDescifrar.Name = "LDescifrar"
        Me.LDescifrar.Size = New System.Drawing.Size(140, 36)
        Me.LDescifrar.TabIndex = 16
        Me.LDescifrar.Text = "Descifrar"
        Me.LDescifrar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PBIcon
        '
        Me.PBIcon.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PBIcon.Enabled = False
        Me.PBIcon.Image = Global.CCV_Crypt.My.Resources.Resources.icono
        Me.PBIcon.Location = New System.Drawing.Point(238, 18)
        Me.PBIcon.Name = "PBIcon"
        Me.PBIcon.Size = New System.Drawing.Size(120, 62)
        Me.PBIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBIcon.TabIndex = 0
        Me.PBIcon.TabStop = False
        '
        'PBSpinner
        '
        Me.PBSpinner.Cursor = System.Windows.Forms.Cursors.Default
        Me.PBSpinner.Image = Global.CCV_Crypt.My.Resources.Resources.spinner
        Me.PBSpinner.Location = New System.Drawing.Point(267, 18)
        Me.PBSpinner.Name = "PBSpinner"
        Me.PBSpinner.Size = New System.Drawing.Size(62, 62)
        Me.PBSpinner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBSpinner.TabIndex = 17
        Me.PBSpinner.TabStop = False
        Me.PBSpinner.Visible = False
        '
        'T1
        '
        Me.T1.Interval = 20
        '
        'ProgressBarT1
        '
        Me.ProgressBarT1.Location = New System.Drawing.Point(-1, 0)
        Me.ProgressBarT1.Name = "ProgressBarT1"
        Me.ProgressBarT1.Size = New System.Drawing.Size(586, 4)
        Me.ProgressBarT1.TabIndex = 20
        '
        'RTBOutAux
        '
        Me.RTBOutAux.BackColor = System.Drawing.Color.White
        Me.RTBOutAux.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBOutAux.DetectUrls = False
        Me.RTBOutAux.ForeColor = System.Drawing.Color.DimGray
        Me.RTBOutAux.Location = New System.Drawing.Point(17, 452)
        Me.RTBOutAux.MaxLength = 1000000
        Me.RTBOutAux.Name = "RTBOutAux"
        Me.RTBOutAux.ReadOnly = True
        Me.RTBOutAux.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.RTBOutAux.Size = New System.Drawing.Size(56, 22)
        Me.RTBOutAux.TabIndex = 21
        Me.RTBOutAux.Text = ""
        Me.RTBOutAux.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(584, 481)
        Me.Controls.Add(Me.RTBOutAux)
        Me.Controls.Add(Me.ProgressBarT1)
        Me.Controls.Add(Me.PBSpinner)
        Me.Controls.Add(Me.LDescifrar)
        Me.Controls.Add(Me.LCifrar)
        Me.Controls.Add(Me.LTextGenerated)
        Me.Controls.Add(Me.LKey)
        Me.Controls.Add(Me.LCharIn)
        Me.Controls.Add(Me.RTBIn)
        Me.Controls.Add(Me.PBIcon)
        Me.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(600, 520)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(600, 520)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CCV Crypt"
        CType(Me.PBIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBSpinner, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PBIcon As PictureBox
    Friend WithEvents RTBIn As RichTextBox
    Friend WithEvents LCharIn As Label
    Friend WithEvents LKey As Label
    Friend WithEvents LTextGenerated As Label
    Friend WithEvents LCifrar As Label
    Friend WithEvents LDescifrar As Label
    Friend WithEvents PBSpinner As PictureBox
    Friend WithEvents T1 As Timer
    Friend WithEvents ProgressBarT1 As ProgressBar
    Friend WithEvents RTBOutAux As RichTextBox
End Class
