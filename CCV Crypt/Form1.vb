﻿Imports System.Math

Public Class Form1
    Dim action As String 'Acción a tomar
    Dim vChars As String 'Cadena con carácteres permitidos
    Dim vvChars As String 'Cadena con carácteres permitidos, alterada por auxstrFinal
    Dim auxstrFinal As String 'Cadena con llave procesada y números de ordenamiento
    Dim strSustBin As String 'Cadena de carácteres para sustituir el binario

    Private Sub Home_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        vChars = "1234567890ABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚabcdefghijklmnñopqrstuvwxyzáéíóú.,:;-_!¡¿?/\()[]{}@#|$%&=*^<> +~'`´¬"
    End Sub

    Public Sub Start_Work()
        Call Post(0)
        T1.Enabled = True
        If action = "cif" Then 'Proceso de codificación
            Out.RTBOut.Text = RTBIn.Text
            GetNumbers()
            UpsetvChars()
            SelectSustBin()
            Dim fin1 As Integer
            fin1 = Microsoft.VisualBasic.Mid(auxstrFinal, 1, 1)
            For i = 1 To fin1 Step 1
                Out.RTBOut.Text = OrderPairOdd(Out.RTBOut.Text, Microsoft.VisualBasic.Mid(auxstrFinal, i, 1))
            Next
            Out.RTBOut.Text = MoveRight(Out.RTBOut.Text, Microsoft.VisualBasic.Mid(auxstrFinal, 2, 1))
            Out.RTBOut.Text = InvText(Out.RTBOut.Text)
            Out.RTBOut.Text = MoveLeft(Out.RTBOut.Text, Microsoft.VisualBasic.Mid(auxstrFinal, 3, 1))
            Out.RTBOut.Text = FiveColumns(Out.RTBOut.Text)
            Out.RTBOut.Text = ConvAltBinChars(Out.RTBOut.Text)
            Out.RTBOut.Text = SustBin(Out.RTBOut.Text)
            Out.RTBOut.Text = HideSustBin(Out.RTBOut.Text)
            RTBOutAux.Text = Out.RTBOut.Text
        Else 'Proceso de decodificación
            Out.RTBOut.Text = RTBIn.Text
            GetNumbers()
            UpsetvChars()
            SelectSustBin()
            Out.RTBOut.Text = ShowSustBin(Out.RTBOut.Text)
            Out.RTBOut.Text = InvSustBin(Out.RTBOut.Text)
            Out.RTBOut.Text = SepStrSpace(Out.RTBOut.Text)
            Out.RTBOut.Text = InvFiveColumns(Out.RTBOut.Text)
            Out.RTBOut.Text = MoveRight(Out.RTBOut.Text, Microsoft.VisualBasic.Mid(auxstrFinal, 3, 1))
            Out.RTBOut.Text = InvText(Out.RTBOut.Text)
            Out.RTBOut.Text = MoveLeft(Out.RTBOut.Text, Microsoft.VisualBasic.Mid(auxstrFinal, 2, 1))
            Dim fin2 As Integer
            fin2 = Microsoft.VisualBasic.Mid(auxstrFinal, 1, 1)
            For i = fin2 To 1 Step -1
                Out.RTBOut.Text = InvOrderPairOdd(Out.RTBOut.Text, Microsoft.VisualBasic.Mid(auxstrFinal, i, 1))
            Next
            RTBOutAux.Text = Out.RTBOut.Text
        End If
    End Sub

    Function Dec2Bin(ByVal nDec As Integer) 'Función para convertir de decimal a binario
        Try
            Dim strAcum, resul As String
            Dim centinela = 0
            strAcum = "" : resul = ""
            While centinela = 0
                If nDec <= 1 Then
                    strAcum = strAcum & nDec
                    centinela = 1
                Else
                    strAcum = strAcum & nDec Mod 2
                    nDec = Fix(nDec / 2) 'Redondear hacia abajo
                End If
            End While
            For i = Len(strAcum) To 1 Step -1
                resul = resul & Microsoft.VisualBasic.Mid(strAcum, i, 1)
            Next
            Return (resul)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function Bin2Dec(ByVal nBin As String) 'Función para convertir de binario a decimal
        Try
            Dim strAcum, aux As Integer
            strAcum = 0 : aux = 0
            For i = Len(nBin) To 1 Step -1
                strAcum = strAcum + (Microsoft.VisualBasic.Mid(nBin, i, 1) * (2 ^ aux))
                aux += 1
            Next
            Return (strAcum)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Private Sub GetNumbers() 'Función para procesar la llave
        Dim Mkey(10, 12) As String 'Matriz de pasos del proceso de la llave
        'Guardar caracteres de la llave en la fila 0
        For i = 1 To 10 Step 1
            Mkey(i, 0) = Microsoft.VisualBasic.Mid(LKey.Text, i, 1)
        Next
        LKey.Text = ""
        'Sacar la tangente de cada numero y guardarlo en fila 1
        For i = 1 To 10 Step 1
            Mkey(i, 1) = Tan(Mkey(i, 0) * (PI / 180)) 'Convertir de tangente en radianes a grados sexagesimales
        Next
        'Guardar solo los primeros 5 dígitos a la derecha del punto en fila 2
        For i = 1 To 10 Step 1
            Mkey(i, 2) = Microsoft.VisualBasic.Mid(Mkey(i, 1), 3, 5)
        Next
        'Sumar los carácteres del resultado y guardarlo en fila 3
        Dim sumChar2 As Integer
        For i = 1 To 10 Step 1
            sumChar2 = 0
            For j = 1 To 5 Step 1
                sumChar2 = sumChar2 + Microsoft.VisualBasic.Mid(Mkey(i, 2), j, 1)
            Next
            Mkey(i, 3) = sumChar2
        Next
        'Reducir el resultado a un único dígito y guardarlo en fila 4
        Dim sumChar3 As Integer
        For i = 1 To 10 Step 1
            sumChar3 = 0
            For j = 1 To Len(Mkey(i, 3)) Step 1
                sumChar3 = sumChar3 + Microsoft.VisualBasic.Mid(Mkey(i, 3), j, 1)
            Next
            Mkey(i, 4) = sumChar3
        Next
        'Seguir reduciendo hasta 8 veces y guardarlo en fila 4
        Dim sumChar4, aux4 As Integer
        aux4 = 0
        While aux4 < 8
            For i = 1 To 10 Step 1
                sumChar4 = 0
                For j = 1 To Len(Mkey(i, 4)) Step 1
                    sumChar4 = sumChar4 + Microsoft.VisualBasic.Mid(Mkey(i, 4), j, 1)
                Next
                Mkey(i, 4) = sumChar4
            Next
            aux4 += 1
        End While
        'Sumar el 1°+2°, 2°+3°, 3°+4°, ... y 10°+1° y guardarlo en fila 5
        For i = 1 To 10 Step 1
            If i = 10 Then
                Mkey(i, 5) = Mkey(i, 4) + Mkey(1, 4)
            Else
                Mkey(i, 5) = Mkey(i, 4) + Mkey(i + 1, 4)
            End If
        Next
        'Reducir 1 vez y guardarlo en fila 6
        Dim sumChar5 As Integer
        For i = 1 To 10 Step 1
            sumChar5 = 0
            For j = 1 To Len(Mkey(i, 5)) Step 1
                sumChar5 = sumChar5 + Microsoft.VisualBasic.Mid(Mkey(i, 5), j, 1)
            Next
            Mkey(i, 6) = sumChar5
        Next
        'Obtener el logarito natural y guardarlo en fila 7
        For i = 1 To 10 Step 1
            Mkey(i, 7) = Log(Mkey(i, 6))
        Next
        'Guardar el entero y algunos numeros a la derecha del punto en fila 8
        For i = 1 To 10 Step 1
            Mkey(i, 8) = Microsoft.VisualBasic.Mid(Mkey(i, 7), 1, 1) & ","
            If Mkey(i, 6) Mod 2 = 0 Then
                For j = 4 To 8 Step 2
                    Mkey(i, 8) = Mkey(i, 8) & Microsoft.VisualBasic.Mid(Mkey(i, 7), j, 1)
                Next
            Else
                For j = 3 To 7 Step 2
                    Mkey(i, 8) = Mkey(i, 8) & Microsoft.VisualBasic.Mid(Mkey(i, 7), j, 1)
                Next
            End If
        Next
        'Alterar los numeros a la derecha de la coma segun el primer dígito y guardarlos en fila 9
        Dim aux9 As String
        For i = 1 To 10 Step 1
            aux9 = ""
            Select Case Microsoft.VisualBasic.Mid(Mkey(i, 8), 1, 1)
                Case 1
                    For j = 5 To 3 Step -1
                        Mkey(i, 9) = Mkey(i, 9) & Microsoft.VisualBasic.Mid(Mkey(i, 8), j, 1)
                    Next
                Case 2
                    For j = 5 To 3 Step -1
                        aux9 = aux9 & Microsoft.VisualBasic.Mid(Mkey(i, 8), j, 1)
                    Next
                    Mkey(i, 9) = Microsoft.VisualBasic.Mid(aux9, 1, 1) & Microsoft.VisualBasic.Mid(aux9, 3, 1) & Microsoft.VisualBasic.Mid(aux9, 2, 1)
            End Select
        Next
        'Concatenar y eliminar los ceros
        Dim auxstr12, auxstr13, auxstr14 As String
        auxstr12 = "" : auxstr13 = "" : auxstr14 = ""
        auxstrFinal = ""
        For i = 1 To 10 Step 1
            auxstr12 = auxstr12 & Mkey(i, 9)
        Next
        For i = 1 To Len(auxstr12) Step 1
            If Not Microsoft.VisualBasic.Mid(auxstr12, i, 1) = 0 Then
                auxstr13 = auxstr13 & Microsoft.VisualBasic.Mid(auxstr12, i, 1)
            End If
        Next
        If Len(auxstr13) < 10 Then
            auxstr13 = auxstr13 & auxstr13
        End If
        For i = 10 To 3 Step -1
            auxstr14 = auxstr14 & Microsoft.VisualBasic.Mid(auxstr13, i, 1) & Mkey(i, 0)
        Next
        If Len(auxstr14) < 10 Then
            auxstr14 = auxstr14 & auxstr14
        End If
        For i = 1 To 14 Step 1
            auxstrFinal = auxstrFinal & Microsoft.VisualBasic.Mid(auxstr14, i, 1)
        Next
    End Sub

    Private Sub UpsetvChars() 'Función para alterar la cadena de carácteres permitidos
        vvChars = vChars
        Dim auxStr1, auxStr2, auxStr3, blockquote(11, 3), strConca As String
        Dim aux1, sum As Integer
        aux1 = 1 : strConca = "" : auxStr1 = "123456789ab"
        For i = 1 To Len(vvChars) Step 1
            blockquote(aux1, 0) = blockquote(aux1, 0) & Microsoft.VisualBasic.Mid(vvChars, i, 1)
            If i Mod 10 = 0 Then
                aux1 += 1
            End If
        Next
        For i = 1 To 11 Step 1
            If Microsoft.VisualBasic.Mid(auxstrFinal, i, 1) Mod 2 = 0 Then
                blockquote(i, 1) = MoveRight(blockquote(i, 0), Microsoft.VisualBasic.Mid(auxstrFinal, i, 1))
            Else
                blockquote(i, 1) = MoveLeft(blockquote(i, 0), Microsoft.VisualBasic.Mid(auxstrFinal, i, 1))
            End If
        Next
        For i = 1 To 11 Step 1
            blockquote(i, 2) = OrderPairOdd(blockquote(i, 1), Microsoft.VisualBasic.Mid(auxstrFinal, i, 1))
        Next
        If Microsoft.VisualBasic.Mid(auxstrFinal, 12, 1) Mod 2 = 0 Then
            auxStr2 = MoveRight(auxStr1, Microsoft.VisualBasic.Mid(auxstrFinal, 12, 1))
        Else
            auxStr2 = MoveRight(auxStr1, Microsoft.VisualBasic.Mid(auxstrFinal, 12, 1))
        End If
        auxStr3 = OrderPairOdd(auxStr2, Microsoft.VisualBasic.Mid(auxstrFinal, 12, 1))
        auxStr3 = OrderPairOdd(auxStr3, Microsoft.VisualBasic.Mid(auxstrFinal, 13, 1))
        auxStr3 = OrderPairOdd(auxStr3, Microsoft.VisualBasic.Mid(auxstrFinal, 14, 1))
        For i = 1 To 11 Step 1
            If Microsoft.VisualBasic.Mid(auxStr3, i, 1) = "a" Then
                blockquote(i, 3) = blockquote(10, 2)
            Else
                If Microsoft.VisualBasic.Mid(auxStr3, i, 1) = "b" Then
                    blockquote(i, 3) = blockquote(11, 2)
                Else
                    blockquote(i, 3) = blockquote(Microsoft.VisualBasic.Mid(auxStr3, i, 1), 2)
                End If
            End If
        Next
        For i = 1 To 11 Step 1
            strConca = strConca & blockquote(i, 3)
        Next
        sum = Microsoft.VisualBasic.Mid(auxstrFinal, 13, 1) + Microsoft.VisualBasic.Mid(auxstrFinal, 14, 1)
        strConca = OrderPairOdd(strConca, sum)
        vvChars = strConca
    End Sub

    Function MoveRight(ByVal str As String, ByVal n As Integer) 'Función para recorrer una cadena a la derecha
        Try
            Dim aux, strFin As String
            For k = 1 To n Step 1
                strFin = Microsoft.VisualBasic.Mid(str, Len(str), 1)
                For i = 1 To Len(str) - 1 Step 1
                    aux = Microsoft.VisualBasic.Mid(str, i, 1)
                    strFin = strFin & aux
                Next
                str = strFin
            Next
            Return (str)
        Catch ex As Exception
            Return ("")
        End Try

    End Function

    Function MoveLeft(ByVal str As String, ByVal n As Integer) 'Función para recorrer una cadena a la izquierda
        Try
            Dim aux, strFin, keep As String
            For k = 1 To n Step 1
                strFin = ""
                keep = Microsoft.VisualBasic.Mid(str, 1, 1)
                For i = 2 To Len(str) Step 1
                    aux = Microsoft.VisualBasic.Mid(str, i, 1)
                    strFin = strFin & aux
                Next
                str = strFin & keep
            Next
            Return (str)
        Catch ex As Exception
            Return ("")
        End Try

    End Function

    Function OrderPairOdd(ByVal str As String, ByVal n As Integer) 'Función para ordenar una cadena con el método par-impar
        Try
            Dim aux1, aux2, strFin As String
            aux1 = "" : aux2 = "" : strFin = ""
            For i = 1 To Len(str) Step 1
                If i Mod 2 = 0 Then
                    aux2 = aux2 & Microsoft.VisualBasic.Mid(str, i, 1)
                Else
                    aux1 = aux1 & Microsoft.VisualBasic.Mid(str, i, 1)
                End If
            Next
            If n Mod 2 = 0 Then
                strFin = aux2 & aux1
            Else
                strFin = aux1 & aux2
            End If
            Return (strFin)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function InvOrderPairOdd(ByVal str As String, ByVal n As Integer) 'Función para invertir la función OrderPairOdd
        Try
            Dim strFinal, nums, STRpar, STRimpar As String
            Dim par, impar As Integer
            nums = Microsoft.VisualBasic.Mid(auxstrFinal, 1, Microsoft.VisualBasic.Mid(auxstrFinal, 1, 1)) : strFinal = ""
            If n Mod 2 = 0 Then
                If Len(str) Mod 2 = 0 Then
                    par = Len(str) / 2 : impar = Len(str) - par
                    STRpar = Microsoft.VisualBasic.Mid(str, 1, par) : STRimpar = Microsoft.VisualBasic.Mid(str, par + 1, impar)
                    For i = 1 To par Step 1
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(STRimpar, i, 1)
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(STRpar, i, 1)
                    Next
                Else
                    par = Fix(Len(str) / 2) : impar = Len(str) - par
                    STRpar = Microsoft.VisualBasic.Mid(str, 1, par) : STRimpar = Microsoft.VisualBasic.Mid(str, par + 1, impar)
                    For i = 1 To impar Step 1
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(STRimpar, i, 1)
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(STRpar, i, 1)
                    Next
                End If
            Else
                If Len(str) Mod 2 = 0 Then
                    par = Len(str) / 2 : impar = Len(str) - par
                    STRimpar = Microsoft.VisualBasic.Mid(str, 1, impar) : STRpar = Microsoft.VisualBasic.Mid(str, impar + 1, par)
                    For i = 1 To par Step 1
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(STRimpar, i, 1)
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(STRpar, i, 1)
                    Next
                Else
                    par = (Len(str) / 2) - 0.5 : impar = (Len(str) - par)
                    STRimpar = Microsoft.VisualBasic.Mid(str, 1, impar) : STRpar = Microsoft.VisualBasic.Mid(str, impar + 1, par)
                    For i = 1 To impar Step 1
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(STRimpar, i, 1)
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(STRpar, i, 1)
                    Next
                End If
            End If
            Return (strFinal)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function InvText(ByVal str As String) 'Función para invertir el texto
        Try
            Dim strFin As String
            strFin = ""
            For i = Len(str) To 1 Step -1
                strFin = strFin & Microsoft.VisualBasic.Mid(str, i, 1)
            Next
            Return (strFin)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function FiveColumns(ByVal str As String) 'Función para ordenar por 5 columnas
        Try
            Dim keep(5), strFin As String
            Dim aux As Integer
            aux = 0 : strFin = ""
            For i = 1 To Len(str) Step 1
                aux += 1
                Select Case aux
                    Case 1
                        keep(aux) = keep(aux) & Microsoft.VisualBasic.Mid(str, i, 1)
                    Case 2
                        keep(aux) = keep(aux) & Microsoft.VisualBasic.Mid(str, i, 1)
                    Case 3
                        keep(aux) = keep(aux) & Microsoft.VisualBasic.Mid(str, i, 1)
                    Case 4
                        keep(aux) = keep(aux) & Microsoft.VisualBasic.Mid(str, i, 1)
                    Case 5
                        keep(aux) = keep(aux) & Microsoft.VisualBasic.Mid(str, i, 1)
                        aux = 0
                End Select
            Next
            For i = 1 To 5 Step 1
                strFin = strFin & keep(i)
            Next
            Return (strFin)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function InvFiveColumns(ByVal str As String) 'Función para invertir el orden de 5 columnas
        Try
            Dim strFinal, blocks(5), sizeF As String
            Dim size, mood, ini, aux1 As Integer
            strFinal = "" : size = Fix(Len(str) / 5) : mood = Len(str) Mod 5 : aux1 = 0 : sizeF = "" : ini = 1
            For i = 1 To 5 Step 1
                mood -= 1
                If mood >= 0 Then
                    sizeF = (size + 1)
                Else
                    sizeF = size
                End If
                blocks(i) = blocks(i) & Microsoft.VisualBasic.Mid(str, ini, sizeF)
                ini = ini + sizeF
            Next
            For i = 1 To size + 1 Step 1
                For j = 1 To 5 Step 1
                    strFinal = strFinal & Microsoft.VisualBasic.Mid(blocks(j), i, 1)
                Next
            Next
            Return (strFinal)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function ConvAltBinChars(ByVal str As String) 'Función para remplazar texto con vvChars, alterar números y llamar para convertir a Dec2Bin
        Try
            Dim strFinal As String
            Dim aux1, aux2, auxF As Integer
            strFinal = "" : aux1 = 1 : aux1 = 0
            For i = 1 To Len(str) Step 1
                aux1 += 1
                aux2 = FindNum(Microsoft.VisualBasic.Mid(str, i, 1), 2) + Microsoft.VisualBasic.Mid(auxstrFinal, aux1, 1)
                auxF = Dec2Bin(aux2)
                strFinal = strFinal & auxF & " "
                If aux1 = 14 Then
                    aux1 = 0
                End If
            Next
            Return (strFinal)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function FindNum(ByVal c As String, ByVal z As Integer) 'Función para buscar la posición de acuerdo a un carácter en vChars o vvChars
        Try
            Select Case z
                Case 1
                    For i = 1 To Len(vChars) Step 1
                        If c = Microsoft.VisualBasic.Mid(vChars, i, 1) Then
                            Return (i)
                        End If
                    Next
                Case 2
                    For i = 1 To Len(vvChars) Step 1
                        If c = Microsoft.VisualBasic.Mid(vvChars, i, 1) Then
                            Return (i)
                        End If
                    Next
            End Select
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function FindChar(ByVal n As Integer, ByVal z As Integer) 'Función para buscar un carácter de acuerdo a una posición en vChars o vvChars
        Try
            Dim c As String
            c = ""
            Select Case z
                Case 1
                    c = Microsoft.VisualBasic.Mid(vChars, n, 1)
                Case 2
                    c = Microsoft.VisualBasic.Mid(vvChars, n, 1)
            End Select
            Return (c)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Private Sub SelectSustBin() 'Función para elegir los carácteres de sustitución del binario
        Try
            Dim zero, one, space As Integer
            Dim SustZero, SustOne, SustSpace As String
            zero = Microsoft.VisualBasic.Mid(auxstrFinal, 12, 1)
            one = Microsoft.VisualBasic.Mid(auxstrFinal, 13, 1)
            space = Microsoft.VisualBasic.Mid(auxstrFinal, 14, 1)
            one = one + zero : space = space + zero + one
            SustZero = FindChar(zero, 2) : SustOne = FindChar(one, 2) : SustSpace = FindChar(space, 2)
            strSustBin = SustZero & SustOne & SustSpace
        Catch ex As Exception

        End Try
    End Sub

    Function SustBin(ByVal str As String) 'Función para sustituir carácteres del binario
        Try
            Dim strFinal As String
            strFinal = ""
            For i = 1 To Len(str) Step 1
                Select Case Microsoft.VisualBasic.Mid(str, i, 1)
                    Case "0"
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(strSustBin, 1, 1)
                    Case "1"
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(strSustBin, 2, 1)
                    Case " "
                        strFinal = strFinal & Microsoft.VisualBasic.Mid(strSustBin, 3, 1)
                End Select
            Next
            Return (strFinal)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function InvSustBin(ByVal str As String) 'Función para sustituir carácteres al binario
        Try
            Dim strFinal As String
            strFinal = ""
            For i = 1 To Len(str) Step 1
                Select Case Microsoft.VisualBasic.Mid(str, i, 1)
                    Case Microsoft.VisualBasic.Mid(strSustBin, 1, 1)
                        strFinal = strFinal & "0"
                    Case Microsoft.VisualBasic.Mid(strSustBin, 2, 1)
                        strFinal = strFinal & "1"
                    Case Microsoft.VisualBasic.Mid(strSustBin, 3, 1)
                        strFinal = strFinal & " "
                End Select
            Next
            Return (strFinal)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function SepStrSpace(ByVal str As String) 'Separar cadena por espacios y llamar a Bin2Dec
        Try
            Dim ini, aux, aux2, a, b As Integer
            Dim strF, strFinal As String
            aux = 0 : aux2 = 0 : ini = 1 : strF = "" : strFinal = ""
            For i = 1 To Len(str) Step 1
                aux += 1
                If Microsoft.VisualBasic.Mid(str, i, 1) = " " Then
                    strF = Microsoft.VisualBasic.Mid(str, ini, aux)
                    ini = i + 1 : aux = 0 : aux2 += 1
                    a = Bin2Dec(Microsoft.VisualBasic.Mid(strF, 1, Len(strF) - 1))
                    b = Microsoft.VisualBasic.Mid(auxstrFinal, aux2, 1)
                    strFinal = strFinal & FindChar(a - b, 2)
                    If aux2 = 14 Then
                        aux2 = 0
                    End If
                End If
            Next
            Return (strFinal)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function HideSustBin(ByVal str As String) 'Función para ofuscar texto
        Try
            Dim strFinal As String
            Dim vNum(14), aux As Integer
            strFinal = "" : aux = 0
            For i = 1 To 14 Step 1
                vNum(i) = Microsoft.VisualBasic.Mid(auxstrFinal, i, 1)
            Next
            For i = 1 To Len(str) Step 1
                aux += 1
                strFinal = strFinal & Microsoft.VisualBasic.Mid(vvChars, vNum(aux), vNum(aux)) & Microsoft.VisualBasic.Mid(str, i, 1)
                If aux = 14 Then
                    aux = 0
                End If
            Next
            Return (strFinal)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Function ShowSustBin(ByVal str As String) 'Función para des-ofuscar texto
        Try
            Dim strFinal As String
            Dim vNum(14), aux, auxi As Integer
            strFinal = "" : aux = 0 : auxi = 0
            For i = 1 To 14 Step 1
                vNum(i) = Microsoft.VisualBasic.Mid(auxstrFinal, i, 1)
            Next
            For i = 1 To Len(str) Step 1
                aux += 1
                auxi = auxi + vNum(aux) + 1
                strFinal = strFinal & Microsoft.VisualBasic.Mid(str, auxi, 1)
                If aux = 14 Then
                    aux = 0
                End If
            Next
            Return (strFinal)
        Catch ex As Exception
            Return ("")
        End Try
    End Function

    Private Sub RTBIn_TextChanged(sender As Object, e As EventArgs) Handles RTBIn.TextChanged
        LCharIn.Text = "Caracteres: " & Len(RTBIn.Text)
    End Sub

    Private Sub LCifrar_Click(sender As Object, e As EventArgs) Handles LCifrar.Click
        action = "cif"
        PBIcon.Enabled = True
    End Sub

    Private Sub LCifrar_MouseEnter(sender As Object, e As EventArgs) Handles LCifrar.MouseEnter
        LCifrar.ForeColor = Color.DodgerBlue
    End Sub

    Private Sub LCifrar_MouseLeave(sender As Object, e As EventArgs) Handles LCifrar.MouseLeave
        If action = "cif" Then
            LCifrar.ForeColor = Color.DodgerBlue
            LDescifrar.ForeColor = Color.DimGray
        Else
            If action = "des" Then
                LCifrar.ForeColor = Color.DimGray
                LDescifrar.ForeColor = Color.DodgerBlue
            Else
                LCifrar.ForeColor = Color.DimGray
                LDescifrar.ForeColor = Color.DimGray
            End If
        End If
    End Sub

    Private Sub LDescifrar_Click(sender As Object, e As EventArgs) Handles LDescifrar.Click
        action = "des"
        PBIcon.Enabled = True
    End Sub

    Private Sub LDescifrar_MouseEnter(sender As Object, e As EventArgs) Handles LDescifrar.MouseEnter
        LDescifrar.ForeColor = Color.DodgerBlue
    End Sub

    Private Sub LDescifrar_MouseLeave(sender As Object, e As EventArgs) Handles LDescifrar.MouseLeave
        If action = "cif" Then
            LCifrar.ForeColor = Color.DodgerBlue
            LDescifrar.ForeColor = Color.DimGray
        Else
            If action = "des" Then
                LCifrar.ForeColor = Color.DimGray
                LDescifrar.ForeColor = Color.DodgerBlue
            Else
                LCifrar.ForeColor = Color.DimGray
                LDescifrar.ForeColor = Color.DimGray
            End If
        End If
    End Sub

    Private Sub PBIcon_Click(sender As Object, e As EventArgs) Handles PBIcon.Click
        If Round(Len(RTBIn.Text) / 20) >= 0 Then 'Ajustar Temporizador según caracteres
            T1.Interval = 1
        Else
            T1.Interval = Round(Len(RTBIn.Text) / 20)
        End If

        If Len(RTBIn.Text) > 2000 And action = "cif" Then
            MsgBox("El texto a cifrar debe tener máximo 2000 caracteres", MsgBoxStyle.Exclamation, "CCV Crypt")
        Else
            If Len(RTBIn.Text) = 0 Then
                If action = "cif" Then
                    MsgBox("No hay texto para cifrar", MsgBoxStyle.Exclamation, "CCV Crypt")
                End If
                If action = "des" Then
                    MsgBox("No hay texto para descifrar", MsgBoxStyle.Exclamation, "CCV Crypt")
                End If
            Else
                RTBOutAux.Text = ""
                Me.Enabled = False
                If action = "cif" Then
                    Key.LBuild.Text = "Cifrar"
                Else
                    Key.LBuild.Text = "Descifrar"
                End If
                Key.Show()
            End If
        End If
    End Sub

    Private Sub LTextGenerated_Click(sender As Object, e As EventArgs) Handles LTextGenerated.Click
        Out.RTBOut.Text = RTBOutAux.Text
        Out.Show()
    End Sub

    Private Sub T1_Tick(sender As Object, e As EventArgs) Handles T1.Tick
        ProgressBarT1.Increment(1)
        If ProgressBarT1.Value = 99 Then
            ProgressBarT1.Value = 1
            T1.Enabled = False
            Call Post(1)
        End If
    End Sub

    Function Post(ByVal n As Integer)
        Select Case n
            Case 0
                PBIcon.Visible = False
                LCifrar.Enabled = False
                LDescifrar.Enabled = False
                LTextGenerated.Visible = False
                RTBIn.ReadOnly = True
                PBSpinner.Visible = True
            Case 1
                PBIcon.Visible = True
                LCifrar.Enabled = True
                LDescifrar.Enabled = True
                LTextGenerated.Visible = True
                RTBIn.ReadOnly = False
                PBSpinner.Visible = False
        End Select
    End Function

End Class