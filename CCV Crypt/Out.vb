﻿Public Class Out

    Private Sub Out_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LCharOut.Text = "Caracteres: " & Len(RTBOut.Text)
        If Len(RTBOut.Text) = 0 Then
            MsgBox("LLave incorrecta", MsgBoxStyle.Exclamation, "CCV Crypt")
        End If
    End Sub

    Private Sub BCopiar_Click(sender As Object, e As EventArgs) Handles BCopiar.Click
        Clipboard.SetText(RTBOut.Text)
    End Sub

End Class