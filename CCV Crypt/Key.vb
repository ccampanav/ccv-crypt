﻿Public Class Key
    Private Sub Key_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Form1.Enabled = True
    End Sub

    Private Sub Key_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub TBKey_TextChanged(sender As Object, e As EventArgs) Handles TBKey.TextChanged
        LCharKey.Text = "Caracteres: " & Len(TBKey.Text)
        If Len(TBKey.Text) = 10 Then
            LBuild.Enabled = True
        Else
            LBuild.Enabled = False
        End If
    End Sub

    Private Sub TBKey_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TBKey.KeyPress
        Dim nfiltro As String
        nfiltro = "123456789"
        If InStr(nfiltro, e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub LCancelar_Click(sender As Object, e As EventArgs) Handles LCancelar.Click
        Me.Close()
    End Sub

    Private Sub LBuild_Click(sender As Object, e As EventArgs) Handles LBuild.Click
        Form1.LKey.Text = TBKey.Text
        Me.Close()
        Call Form1.Start_Work()
    End Sub

    Private Sub CheckBoxShow_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBoxShow.CheckedChanged
        If CheckBoxShow.Checked = True Then
            TBKey.UseSystemPasswordChar = True
        Else
            TBKey.UseSystemPasswordChar = False
        End If
    End Sub

End Class