﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Key
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Key))
        Me.TBKey = New System.Windows.Forms.TextBox()
        Me.LCharIn = New System.Windows.Forms.Label()
        Me.LCancelar = New System.Windows.Forms.Label()
        Me.LBuild = New System.Windows.Forms.Label()
        Me.LCharKey = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.CheckBoxShow = New System.Windows.Forms.CheckBox()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TBKey
        '
        Me.TBKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBKey.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBKey.ForeColor = System.Drawing.Color.DimGray
        Me.TBKey.Location = New System.Drawing.Point(55, 75)
        Me.TBKey.MaxLength = 10
        Me.TBKey.Name = "TBKey"
        Me.TBKey.Size = New System.Drawing.Size(220, 27)
        Me.TBKey.TabIndex = 0
        Me.TBKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TBKey.UseSystemPasswordChar = True
        '
        'LCharIn
        '
        Me.LCharIn.AutoSize = True
        Me.LCharIn.BackColor = System.Drawing.Color.Transparent
        Me.LCharIn.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCharIn.Location = New System.Drawing.Point(92, 34)
        Me.LCharIn.Name = "LCharIn"
        Me.LCharIn.Size = New System.Drawing.Size(148, 22)
        Me.LCharIn.TabIndex = 9
        Me.LCharIn.Text = "Ingresa la llave"
        Me.LCharIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LCancelar
        '
        Me.LCancelar.AutoSize = True
        Me.LCancelar.BackColor = System.Drawing.Color.Transparent
        Me.LCancelar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LCancelar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCancelar.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LCancelar.Location = New System.Drawing.Point(6, 6)
        Me.LCancelar.Name = "LCancelar"
        Me.LCancelar.Size = New System.Drawing.Size(83, 21)
        Me.LCancelar.TabIndex = 10
        Me.LCancelar.Text = "Cancelar"
        Me.LCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBuild
        '
        Me.LBuild.BackColor = System.Drawing.Color.Transparent
        Me.LBuild.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LBuild.Enabled = False
        Me.LBuild.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBuild.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LBuild.Location = New System.Drawing.Point(224, 6)
        Me.LBuild.Name = "LBuild"
        Me.LBuild.Size = New System.Drawing.Size(100, 21)
        Me.LBuild.TabIndex = 11
        Me.LBuild.Text = "Construir"
        Me.LBuild.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LCharKey
        '
        Me.LCharKey.BackColor = System.Drawing.Color.Transparent
        Me.LCharKey.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCharKey.Location = New System.Drawing.Point(222, 126)
        Me.LCharKey.Name = "LCharKey"
        Me.LCharKey.Size = New System.Drawing.Size(100, 16)
        Me.LCharKey.TabIndex = 16
        Me.LCharKey.Text = "Caracteres: 0"
        Me.LCharKey.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.DarkGray
        Me.PictureBox4.Location = New System.Drawing.Point(328, -1)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(2, 153)
        Me.PictureBox4.TabIndex = 15
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.DarkGray
        Me.PictureBox3.Location = New System.Drawing.Point(0, -1)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(2, 153)
        Me.PictureBox3.TabIndex = 14
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.DarkGray
        Me.PictureBox2.Location = New System.Drawing.Point(-1, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(333, 2)
        Me.PictureBox2.TabIndex = 13
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.DarkGray
        Me.PictureBox1.Location = New System.Drawing.Point(-2, 148)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(333, 2)
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'CheckBoxShow
        '
        Me.CheckBoxShow.AutoSize = True
        Me.CheckBoxShow.Checked = True
        Me.CheckBoxShow.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBoxShow.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxShow.Location = New System.Drawing.Point(282, 83)
        Me.CheckBoxShow.Name = "CheckBoxShow"
        Me.CheckBoxShow.Size = New System.Drawing.Size(12, 11)
        Me.CheckBoxShow.TabIndex = 17
        Me.CheckBoxShow.UseVisualStyleBackColor = True
        '
        'Key
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(330, 150)
        Me.Controls.Add(Me.CheckBoxShow)
        Me.Controls.Add(Me.LCharKey)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.LBuild)
        Me.Controls.Add(Me.LCancelar)
        Me.Controls.Add(Me.LCharIn)
        Me.Controls.Add(Me.TBKey)
        Me.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(330, 150)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(330, 150)
        Me.Name = "Key"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CCV Crypt"
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TBKey As TextBox
    Friend WithEvents LCharIn As Label
    Friend WithEvents LCancelar As Label
    Friend WithEvents LBuild As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents LCharKey As Label
    Friend WithEvents CheckBoxShow As CheckBox
End Class
